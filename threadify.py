import threading


threads = list()


def threadify(func):
    """Runs a function as a thread and stores a reference in the threads list."""
    def wrapper(*arguments, **keyword_arguments):

        thread = threading.Thread(target=func, name=func.__name__, args=arguments, kwargs=keyword_arguments)
        threads.append(thread)
        thread.start()
        return thread

    return wrapper

def join():
    """
    Join all threads created using the module.
    """
    while len(threads):
        thread = threads.pop()
        thread.join()