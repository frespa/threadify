import threadify
import time

class Something:

    @threadify.threadify
    def edit(self, number, letter, first=1, second=2):
        time.sleep(number)
        print(f"edit {number} and {letter}")
        print(f"first {first}")
        print(f"second {second}")




if __name__ == "__main__":
    a = Something()

    t = a.edit(5, "a")
    t2 = a.edit(5, "a")
    t3 = a.edit(5, "a")

    print(t)
    print("hello")

    threadify.join()